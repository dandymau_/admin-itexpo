-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2020 at 05:00 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itexpo_2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `eventx`
--

CREATE TABLE `eventx` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mahasiswa_po` int(11) NOT NULL,
  `mahasiswa_ots` int(11) NOT NULL,
  `umum_po` int(11) NOT NULL,
  `umum_ots` int(11) NOT NULL,
  `phase_1` datetime NOT NULL,
  `phase_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventx`
--

INSERT INTO `eventx` (`id`, `name`, `mahasiswa_po`, `mahasiswa_ots`, `umum_po`, `umum_ots`, `phase_1`, `phase_2`) VALUES
(1, 'Networking', 25000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Web Development', 70000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Poster', 25000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Animasi', 25000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Esai', 25000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Webinar', 25000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Workshop', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `eventx_detail`
--

CREATE TABLE `eventx_detail` (
  `id` int(11) NOT NULL,
  `eventx_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventx_detail`
--

INSERT INTO `eventx_detail` (`id`, `eventx_id`, `title`, `description`) VALUES
(1, 1, 'Pendaftaran Peserta', '21 September - 23 Oktober 2020'),
(2, 1, 'Technical Meeting Penyisihan', '30 Oktober 2020'),
(3, 1, 'Tahap Penyisihan', '31 Oktober - 1 November 2020'),
(4, 1, 'Pengumuman Finalis', '10 November 2020'),
(5, 1, 'Konfirmasi Finalis', '10 - 22 November 2020'),
(6, 1, 'Technical Meeting', '24 November 2020'),
(7, 1, 'Tahap Final', '25 November 2020'),
(8, 1, 'Pengumuman Juara', '26 November 2020'),
(9, 2, 'Pendaftaran Peserta', '21 September - 23 Oktober 2020'),
(10, 2, 'Waktu Pengumpulan ', '25 Oktober - 31 Oktober 2020'),
(11, 2, 'Tahap Penilaian Karya', '2 - 6 November 2020'),
(12, 2, 'Rekapitulasi Penilaian', '7 - 9 November 2020'),
(13, 2, 'Pengumuman Finalis', '10 November 2020'),
(14, 2, 'Konfirmasi Finalis', '10 - 12 November 2020'),
(15, 2, 'Technical Meeting', '14 November 2020'),
(16, 2, 'Presentasi Finalis', '16 November 2020'),
(17, 2, 'Pengumuman Juara', '26 November 2020'),
(18, 3, 'Pendaftaran dan Pengumpulan Karya', '21 September - 19 November 2020'),
(19, 3, 'Penjurian', '20 - 25 November 2020'),
(20, 3, 'Pengumuman Pemenang', '26 November 2020'),
(21, 4, 'Pendaftaran Peserta', '21 - 23 Oktober 2020'),
(22, 4, 'Technical Meeting', '24 Oktober 2020'),
(23, 4, 'Tahap 1 (Storyboard)', '25 - 26 Oktober'),
(24, 4, 'Pengumuman Tahap 1', '5 November 2020'),
(25, 4, 'Tahap 2 (Video Animasi)', '7 - 8 November 2020'),
(26, 4, 'Pengumuman Finalis', '11 November 2020'),
(27, 4, 'Konfirmasi Finalis', '11 - 22 November 2020'),
(28, 4, 'Technical Meeting Final', '24 November 2020'),
(29, 4, 'Presentasi Finalis', '25 November 2020'),
(30, 4, 'Pengumuman Juara', '26 November 2020'),
(31, 5, 'Pendaftaran dan Pengumpulan Karya', '21 September - 19 November 2020'),
(32, 5, 'Penjurian', '20 - 25 November 2020'),
(33, 5, 'Pengumuman Pemenang', '26 November 2020'),
(34, 6, 'Pendaftaran dan pembayaran', '26 Oktober - 24 November 2020'),
(35, 6, 'Pelaksanaaan', '26 November 2020'),
(36, 7, 'Pendaftaran dan pembayaran', '21 September - 15 November 2020'),
(37, 7, 'Pelaksanaan', '21 November 2020');

-- --------------------------------------------------------

--
-- Table structure for table `networkx`
--

CREATE TABLE `networkx` (
  `userx_eventx_id` int(11) NOT NULL,
  `questionx_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `network_optionx`
--

CREATE TABLE `network_optionx` (
  `id` int(11) NOT NULL,
  `questionx_id` int(11) NOT NULL,
  `options` varchar(255) NOT NULL,
  `is_true` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `network_questionx`
--

CREATE TABLE `network_questionx` (
  `id` int(11) NOT NULL,
  `type` enum('options','essay','file') NOT NULL,
  `question` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL,
  `download_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `submitx`
--

CREATE TABLE `submitx` (
  `userx_eventx_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submitx`
--

INSERT INTO `submitx` (`userx_eventx_id`, `file`, `link`, `created_at`) VALUES
(1, '', 'https://docs.google.com/document/d/1A0EDg3FDVVo2meXsSc31JeT4exHnKet_0Svm8dB1AW0/edit?usp=sharing', '2020-08-27 11:16:38'),
(20, 'http://localhost:8888/uploads/file/74736829dbd5976e.docx', '', '2020-08-27 11:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `userx_id` int(11) NOT NULL,
  `eventx_id` int(11) NOT NULL,
  `team_member` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `userx_id`, `eventx_id`, `team_member`) VALUES
(1, 17, 2, 'Sunin'),
(2, 17, 2, 'Kaltim');

-- --------------------------------------------------------

--
-- Table structure for table `userx`
--

CREATE TABLE `userx` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status_akun` enum('pelajar','mahasiswa','umum','admin','super_admin') NOT NULL,
  `instansi` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_verified` int(11) NOT NULL,
  `file_ktm` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_verifikasi` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `verified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userx`
--

INSERT INTO `userx` (`id`, `nama`, `tanggal_lahir`, `status_akun`, `instansi`, `kontak`, `email`, `password`, `is_verified`, `file_ktm`, `foto`, `token`, `token_verifikasi`, `created_at`, `verified_at`) VALUES
(1, 'Dandy Maulana', '1998-09-23', 'pelajar', 'Universitas Negeri Jakarta', '082139230782', 'dandymau223@gmail.com', '$2y$10$yn6.OpeuigptrbECyLZkr.aFMoPAf3DOLFg2J.QLaSFW2MmFd8TeS', 1, '', '', 'ZGFuZHltYXUyMjNAZ21haWwuY29taXQzeHBvMjAxOA==', '', '2018-09-20 06:04:58', '0000-00-00 00:00:00'),
(3, 'Muhammad Rayhan Haroki', '1995-01-04', 'pelajar', 'UNJ', '085691055777', 'rayhan.kin@gmail.com', '$2y$10$UAdj.G74.zJdiydqgo0HAOCe6zxdYV2IDIM2DfCA8REaiusuo86uO', 0, '', '', '', 'cmF5aGFuLmtpbkBnbWFpbC5jb21pdDN4cG8yMDE4VmVyaWZpa2FzaQ==', '2018-09-20 11:08:11', '0000-00-00 00:00:00'),
(15, 'Insan Rizky', '1995-11-02', 'pelajar', 'Universitas Indonesia', '087741114505', 'insansan9@gmail.com', '$2y$10$kkuqXnEC8XBbPbXaa.7Uwe4WjTbqNeg6szsCGbs7JucBJqkhUzRAm', 1, '', 'http://localhost/hafiezar.github.io/service/uploads/foto/4c0ffe327bd1eea9.png', 'aW5zYW5zYW45QGdtYWlsLmNvbWl0M3hwbzIwMTg=', '', '2018-09-21 12:24:51', '2018-09-21 12:28:25'),
(17, 'Swardi', '1997-09-30', 'pelajar', 'SMK 26 Jakarta', '089962001001', 'swardyantara@gmail.com', '$2y$10$wCXR/cY8Qr8HzM7Zu0xZzObWWLNJf2.CVGNVcMCXmObUsbuD5J0Oq', 1, 'http://103.8.12.212:33722/uploads/ktm/5f47499492dd1swardyantara@gmail.com.png', 'http://103.8.12.212:33722/uploads/foto/5f4749cb85968swardyantara@gmail.com.png', 'c3dhcmR5YW50YXJhQGdtYWlsLmNvbWl0M3hwbzIwMTg=', '', '2020-08-24 12:56:58', '2020-08-24 12:59:19'),
(20, 'swardimhs', '1997-09-30', 'mahasiswa', 'UNJ', '082350723424', 'swardiantara@gmail.com', '$2y$10$TvkuPML6jBzZuJoqyieTsOv7qoyqrqEh.hCOhalBnjZrbA7xGnUrS', 1, 'http://localhost:8888/uploads/ktm/5f4797247a4edswardiantara@gmail.com.png', '', 'c3dhcmRpYW50YXJhQGdtYWlsLmNvbWl0M3hwbzIwMTg=', '', '2020-08-26 01:14:42', '2020-08-26 01:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `userx_eventx`
--

CREATE TABLE `userx_eventx` (
  `id` int(11) NOT NULL,
  `userx_id` int(11) NOT NULL,
  `eventx_id` int(11) NOT NULL,
  `registration_code` varchar(5) NOT NULL,
  `is_team` int(11) NOT NULL,
  `team_name` varchar(255) NOT NULL,
  `ign` varchar(255) NOT NULL,
  `bukti_bayar` varchar(255) NOT NULL,
  `payment_status` enum('not_paid','wait_verified','paid') NOT NULL,
  `submission` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `paid_at` datetime NOT NULL,
  `is_delete` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userx_eventx`
--

INSERT INTO `userx_eventx` (`id`, `userx_id`, `eventx_id`, `registration_code`, `is_team`, `team_name`, `ign`, `bukti_bayar`, `payment_status`, `submission`, `created_at`, `paid_at`, `is_delete`) VALUES
(1, 17, 1, '00001', 0, '', '', 'http://localhost:8888/uploads/bukti_bayar/5f4796ca69ffcswardyantara@gmail.com.png', 'wait_verified', 0, '2018-09-21 01:25:04', '2020-08-27 06:19:38', '1'),
(2, 17, 2, '00002', 0, '', '', '', 'not_paid', 0, '2018-09-21 01:25:47', '0000-00-00 00:00:00', '1'),
(3, 17, 2, '00003', 0, '', '', '', 'not_paid', 0, '2020-08-26 12:53:02', '0000-00-00 00:00:00', '1'),
(4, 17, 2, '00004', 0, '', '', '', 'not_paid', 0, '2020-08-26 02:49:23', '0000-00-00 00:00:00', '1'),
(5, 17, 3, '00005', 0, '', '', '', 'not_paid', 0, '2020-08-26 05:25:08', '0000-00-00 00:00:00', '1'),
(6, 17, 1, '00006', 0, '', '', 'http://localhost:8888/uploads/bukti_bayar/5f4796ca69ffcswardyantara@gmail.com.png', 'wait_verified', 0, '2020-08-26 10:42:31', '2020-08-27 06:19:38', '1'),
(7, 17, 2, '00007', 0, '', '', '', 'not_paid', 0, '2020-08-26 10:42:53', '0000-00-00 00:00:00', '1'),
(8, 17, 2, '00008', 0, '', '', '', 'not_paid', 0, '2020-08-26 10:55:03', '0000-00-00 00:00:00', '1'),
(9, 17, 2, '00009', 1, 'LaraPro', '', '', 'not_paid', 0, '2020-08-26 11:02:50', '0000-00-00 00:00:00', '1'),
(10, 17, 3, '00010', 0, '', '', '', 'not_paid', 0, '2020-08-26 11:08:52', '0000-00-00 00:00:00', '1'),
(11, 17, 3, '00011', 0, '', '', '', 'not_paid', 0, '2020-08-26 11:09:24', '0000-00-00 00:00:00', '1'),
(12, 17, 3, '00012', 0, '', '', '', 'not_paid', 0, '2020-08-26 11:10:30', '0000-00-00 00:00:00', '1'),
(13, 17, 4, '00013', 0, '', '', 'http://103.8.12.212:33722/uploads/bukti_bayar/5f47470d97dc9swardyantara@gmail.com.png', 'wait_verified', 0, '2020-08-26 11:11:58', '2020-08-27 12:39:25', '1'),
(14, 17, 5, '00014', 0, '', '', '', 'not_paid', 0, '2020-08-26 11:17:59', '0000-00-00 00:00:00', '1'),
(15, 17, 6, '00015', 0, '', '', '', 'not_paid', 0, '2020-08-26 11:36:06', '0000-00-00 00:00:00', '1'),
(16, 17, 7, '00016', 0, '', '', 'http://localhost:8888/uploads/bukti_bayar/5f479a9a9ffb0swardyantara@gmail.com.png', 'wait_verified', 0, '2020-08-26 11:36:20', '2020-08-27 06:35:54', '1'),
(17, 17, 4, '00017', 0, '', '', 'http://103.8.12.212:33722/uploads/bukti_bayar/5f47470d97dc9swardyantara@gmail.com.png', 'paid', 0, '2020-08-27 12:25:20', '2020-08-27 12:39:25', '0'),
(18, 17, 7, '00018', 0, '', '', 'http://localhost:8888/uploads/bukti_bayar/5f479a9a9ffb0swardyantara@gmail.com.png', 'paid', 0, '2020-08-27 12:47:27', '2020-08-27 06:35:54', '0'),
(19, 17, 1, '00019', 0, '', '', 'http://localhost:8888/uploads/bukti_bayar/5f4796ca69ffcswardyantara@gmail.com.png', 'paid', 0, '2020-08-27 06:19:21', '2020-08-27 06:19:38', '0'),
(20, 20, 5, '00020', 0, '', '', 'http://localhost:8888/uploads/bukti_bayar/5f4797374b073swardiantara@gmail.com.png', 'paid', 0, '2020-08-27 06:21:15', '2020-08-27 06:21:27', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `eventx`
--
ALTER TABLE `eventx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventx_detail`
--
ALTER TABLE `eventx_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `networkx`
--
ALTER TABLE `networkx`
  ADD PRIMARY KEY (`userx_eventx_id`,`questionx_id`) USING BTREE;

--
-- Indexes for table `network_optionx`
--
ALTER TABLE `network_optionx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `network_questionx`
--
ALTER TABLE `network_questionx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submitx`
--
ALTER TABLE `submitx`
  ADD PRIMARY KEY (`userx_eventx_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userx`
--
ALTER TABLE `userx`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- Indexes for table `userx_eventx`
--
ALTER TABLE `userx_eventx`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `registration_code` (`registration_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `eventx`
--
ALTER TABLE `eventx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `eventx_detail`
--
ALTER TABLE `eventx_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `network_optionx`
--
ALTER TABLE `network_optionx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `network_questionx`
--
ALTER TABLE `network_questionx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `submitx`
--
ALTER TABLE `submitx`
  MODIFY `userx_eventx_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `userx`
--
ALTER TABLE `userx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `userx_eventx`
--
ALTER TABLE `userx_eventx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
