@extends('app')
@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin-lte')}}/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@section('title', 'REKAP PESERTA')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @elseif (session('failed'))
                <div class="alert alert-danger">
                    {{ session('failed') }}
                </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Peserta</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Peserta</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-title">DataTable with default features</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <select class="custom-select mb-3 col-2" id="eventx">
                                <option value="99">Semua Kegiatan</option>
                                <option value="1">Networking</option>
                                <option value="2">Web Development</option>
                                <option value="3">Desain Poster</option>
                                <option value="4">Animasi</option>
                                <option value="5">Esai</option>
                                <option value="6">Webinar</option>
                                <option value="7">Workshop</option>
                            </select>
                            <table id="tb_peserta" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>Status</th>
                                        <th>Instansi</th>
                                        <th>Lomba</th>
                                        <th>Kontak</th>
                                        <th>Submisi</th>
                                        <th>Waktu Submit</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>Status</th>
                                        <th>Instansi</th>
                                        <th>Lomba</th>
                                        <th>Kontak</th>
                                        <th>Submisi</th>
                                        <th>Waktu Submit</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
        </div>
    </section>
</div>
@endsection
@push('js')
<!-- DataTables -->
<script src="{{asset('admin-lte')}}/datatables/jquery.dataTables.js"></script>
<script src="{{asset('admin-lte')}}/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- page script -->
<script>
    const initializeTable = (response, eventx_id) => {
        let tabelPeserta = $("#tb_peserta");
        let cekTable = $.fn.dataTable.isDataTable(tabelPeserta)
        if (cekTable) {
            tabelPeserta.DataTable().clear().destroy();
        }

        let opsiTabel = {
            data: eventx_id != 99 ? response.data : response.data.filter((item) => item.eventx_id != 6 && item.eventx_id != 7),
            columns: [
                        {
                            data: null,
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: "nama"
                        },
                        {
                            data: "status_akun"
                        },
                        {
                            data: "instansi"
                        },
                        {
                            data: "nama_event"
                        },
                        {
                            data: "kontak"
                        },
                        {   
                            data: "submission",
                            className: "text-center",
                            render: function(data, type, full) {
                                if(data === 0 && full.eventx_id !=6 && full.eventx_id !=7) {
                                    return (
                                        `<a href="#" class="badge badge-danger disabled">Belum Unggah</a>`
                                        // `<a type="button" class="btn btn-danger disabled">Belum Unggah</a>`
                                    );
                                } else if(full.eventx_id !=6 && full.eventx_id !=7) {
                                    if(full.file) {
                                        return (
                                            `<div class="row justify-content-around">
                                                <div class="col border-right">
                                                    <a href=${full.file} target='__blank' class="badge badge-success">Download file</a>
                                                </div>
                                                <div class="col">
                                                    <button onClick=tombolHapus(${full.id}) class="badge badge-danger batal" data-id=${full.id}>Hapus</button>
                                                </div>
                                            </div>`
                                            // `<a href=${full.file ? full.file : full.link} target='__blank' class="btn  btn-success">Link Submisi</a>` 
                                        );
                                    } else {
                                        return (
                                            `<div class="row justify-content-around">
                                                <div class="col border-right">
                                                    <a href=${full.link} target='__blank' class="badge badge-success ">Link Submisi</a>
                                                </div>
                                                <div class="col">
                                                    <button onClick=tombolHapus(${full.id}) class="badge badge-danger batal" data-id=${full.id}>Hapus</button>
                                                </div>
                                            </div>`
                                            // `<a href=${full.file ? full.file : full.link} target='__blank' class="btn  btn-success">Link Submisi</a>` 
                                        );
                                    }
                                } else {
                                    return(
                                        `<a href="#" class="badge badge-success disabled">Non Submisi</a>`
                                    );
                                }
                            }
                        },
                        {
                            data: "created_at"
                        }
                    ]
        };
        tabelPeserta = $("#tb_peserta").DataTable(opsiTabel);
        if(eventx_id != 99) hideColumn(tabelPeserta, 4);
        if(eventx_id == 6 || eventx_id == 7) {
            hideColumn(tabelPeserta, 6);
            hideColumn(tabelPeserta, 7);
        } 
        
    };

    $("#eventx").change(function () {
        eventx_id = $(this).val();
        $.ajax({
            method: "GET",
            url: `/api/peserta/event/${eventx_id}`
        })
        .done(function(response) {
            if(response.code === 200) {
                notification("success", response.message);
                initializeTable(response, eventx_id)
            } else {
                notification("error", response.message);
                initializeTable(response, eventx_id)
            }
        })
        .fail(function(error) {
            notification("error", error.responseJSON.message);
        });
    });

    const tombolHapus = (id) => {
        // alert(`Berhasil klik ${id}`);
        $.ajax({
            method: 'DELETE',
            url: `/api/submission/${id}`
        })
        .done(function(response) {
            notification('success', response.message);
            initializeTable(response, 99);
        })
        .fail(function (error) {
            notification("error", error.responseJSON.message);
        })
    }

    // $(".batal").on("click", function () {
    //     userx_eventx_id = $(this).data('id');
    // });

    window.onload = function () {
        let eventx_id = $("#eventx").val();
        $.ajax({
            method: "GET",
            url: `/api/peserta/event/${eventx_id}`
        })
        .done(function(response) {
            if(response.code === 200) {
                    // notification("success", response.message);
                    initializeTable(response, eventx_id)
            } else {
                // notification("error", response.message);
                initializeTable(response, eventx_id)
            }
        })
        .fail(function(error) {
            notification("error", error.responseJSON.message);
        });
    }

    const hideColumn = (table, index) => {
        let column = table.column(index);
        column.visible(false);
    }
  </script>
@endpush