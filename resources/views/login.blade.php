@include('templates.head')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b> ITExpo</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Masuk untuk mendapat akses!</p>
      @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
      @elseif (session('failed'))
          <div class="alert alert-danger">
              {{ session('failed') }}
          </div>
      @endif

      <form id="form-login" action="{{ route('login') }}" method="post">
        @method('POST')
        @csrf
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
          <!-- /.col -->
          <div class="row justify-content-end">  
              <div class="col-4 ">
                  <button type="submit" class="btn btn-primary btn-block ">Masuk</button>
                </div>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->




@include('templates.foot')
{{-- @push('js')
<script>

    $("#form-login").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url: `http://localhost:8888/login`,
      method: "POST",
      data: $(this).serialize(),
      success: function (response) {
        console.log(response);
        notification("success", response.message);
        localStorage.setItem(KEY, response.data.token);
        window.location='/peserta'
      },
      error: function (err) {
        console.log(err);
        notification("error", err.responseJSON.message);
      },
    });
  });
</script>
@endpush --}}
</html>
