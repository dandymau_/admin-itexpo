@extends('app')
@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin-lte')}}/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@section('title', 'PESERTA')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <span id="role" class="d-none">{{ Auth::user()->status_akun }}</span>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @elseif (session('failed'))
                <div class="alert alert-danger">
                    {{ session('failed') }}
                </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Peserta</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Peserta</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-title">DataTable with default features</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <table id="tb_peserta" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Status</th>
                            <th>Instansi</th>
                            <th>Kontak</th>
                            <th>Email</th>
                            <th>KTM/Pelajar</th>
                            <th>Verifikasi</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Status</th>
                            <th>Instansi</th>
                            <th>Kontak</th>
                            <th>Email</th>
                            <th>KTM/Pelajar</th>
                            <th>Verifikasi</th>
                            </tr>
                            </tfoot>
                        </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
        </div>
    </section>
    <section>
        <!-- Modal Lihat KTM-->
        <div class="modal fade" id="lihat-ktm" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="lihatKtmLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lihatKtmLabel">KTM / Kartu Pelajar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" class="lihat-ktm img-fluid">
                </div>
                <div class="modal-footer">
                    <form id="veriform">
                        {{-- <input type="text" name="submit" value="verified" class="d-none"> --}}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        {{-- <button id="tombolVerif"type="button" data-id='' class="btn btn-primary">Verfikasi</button> --}}
                    </form>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('js')
<!-- DataTables -->
<script src="{{asset('admin-lte')}}/datatables/jquery.dataTables.js"></script>
<script src="{{asset('admin-lte')}}/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- page script -->
<script>
    var role = $("#role").text();
    $.ajax({
        method: "GET",
        url: `/api/peserta/${$('#role').text()}`
    })
    .done(function(response) {
        if(response.code === 200) {
            $("#tb_peserta").DataTable({
                data: response.data,
                columns: [
                    {
                        data: null,
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        data: "nama"
                    },
                    {
                        data: "status_akun"
                    },
                    {
                        data: "instansi"
                    },
                    {
                        data: "kontak"
                    },
                    {
                        data: "email"
                    },
                    {
                        render: function (data, type, full) {
                            return (
                                `<button id="tes" data-id="${full.ue_id}" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-src="${full.file_ktm}" data-target="#lihat-ktm">Lihat KTM</button>`
                            );
                        }
                    },
                    {
                        render: function(data, type, full) {
                            if(full.is_verified == 0) {
                                if(role == "super_admin") {
                                    return `<div class="row justify-content-around">
                                                <div class="col align-item-center">
                                                    <a class="btn btn-primary btn-sm" href="/peserta/ ${full.id} /verifikasi/">Verifikasi</a>
                                                </div>
                                                <div class="col align-item-center">
                                                    <button type="button" class="btn btn-sm btn-danger" onclick="hapusUser(${full.id})">Hapus User</button>
                                                </div>
                                            </div>`
                                } else {
                                    return `<a class="btn btn-primary btn-sm" href="/peserta/${full.id}/verifikasi/">Verifikasi</a>`
                                }
                            } else {
                                if(role == 'super_admin') {
                                    return `<div class="row justify-content-around">
                                                <div class="col align-item-center">
                                                    <a class="btn btn-primary btn-sm disabled" href="/admin/cab/change/${full.id}" disabled>Sudah Terverifikasi</a>
                                                </div>
                                                <div class="col align-item-center">
                                                    <button type="button" class="btn btn-sm btn-danger" onclick="hapusUser(${full.id})">Hapus User</button>
                                                </div>
                                            </div>`
                                } else {
                                    return `<a class="btn btn-primary btn-sm disabled" href="/admin/cab/change/${full.id}" disabled>Sudah Terverifikasi</a>`;
                                }
                            }
                        }
                    }
                ]
            });
        }
    })
    .fail(function() {
        console.log(error);
    });

    function hapusUser(id) {
        $.ajax({
            method: "DELETE",
            url: "/api/peserta/" + id,
            data: role
        }).done(function (response) {
            if(response.code == 200) {
                alert("Berhasil hapus akun!");
            }
            setTimeout(() => {
                location.reload();
            }, 1000);
        }).fail(function(error) {
            alert('gagal hapus akun!');
            setTimeout(() => {
                location.reload();
            }, 1000);
        });
    }

    $('#lihat-ktm').on('show.bs.modal', function(e){
        let tombol = $(e.relatedTarget );
        let alamatGambar = tombol.data('src');
        let modal = $(this);
        // let idVerif = tombol.data('id');
        modal.find('.modal-body img').attr('src', alamatGambar);
        // modal.find('#tombolVerif').data('id', idVerif);
    });
  </script>
@endpush