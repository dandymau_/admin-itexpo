{{-- @extends('app')

@section('title', 'PEMBAYARAN') --}}

@extends('app')
@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin-lte')}}/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@section('title', 'PEMBAYARAN')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @elseif (session('failed'))
                <div class="alert alert-danger">
                    {{ session('failed') }}
                </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Peserta</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Peserta</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-title">Data Pembayaran Peserta</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <table id="tb_pembayaran" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Kegiatan</th>
                            <th>Instansi</th>
                            <th>Kontak</th>
                            <th>Status</th>
                            <th>Verifikasi</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Kegiatan</th>
                            <th>Instansi</th>
                            <th>Kontak</th>
                            <th>Status</th>
                            <th>Verifikasi</th>
                            </tr>
                            </tfoot>
                        </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
        </div>
    </section>

    <section>
        <!-- Modal Verifikasi-->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Bukti Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" class="buktibayar img-fluid">
                </div>
                <div class="modal-footer">
                    <form id="veriform">
                        <input type="text" name="submit" value="verified" class="d-none">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button id="tombolVerif"type="button" data-id='' class="btn btn-primary">Verfikasi</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('js')
<!-- DataTables -->
<script src="{{asset('admin-lte')}}/datatables/jquery.dataTables.js"></script>
<script src="{{asset('admin-lte')}}/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- page script -->
<script>
    function pesan(id){
        Swal.fire({
                title: 'Anda Yakin?',
                text: "Anda tidak dapat membatalkan!",
                icon: 'warning',
                cancelButtonText: 'Batal',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!',
                }).then((result) => {
                    
                if (result.value) {
                    $.ajax({
                        method: "PUT",
                        url: `/api/pembayaran/${id}/cancel`,
                        
                    }).then((result)=> {
                        Swal.fire(
                            'Terhapus!',
                            'Berkas dihapus',
                            'Sukses'
                        )
                    })
            }
        })
    }

    $.ajax({
        method: "GET",
        url: "/api/pembayaran"
    })
    .done(function(response) {
        if(response.code === 200) {
            $("#tb_pembayaran").DataTable({
                data: response.data.filter((event) => event.e_id !=8),
                columns: [
                    {
                        data: null,
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        data: "nama_lengkap"
                    },
                    {
                        data: "nama_event"
                    },
                    {
                        data: "instansi"
                    },
                    {
                        data: "kontak"
                    },
                    {   data: "payment_status",
                        render: function(data, type, full){
                            if(data === "not_paid"){
                                return`<span type="span" class="badge badge-danger">Belum Unggah</span>`
                            }
                            else if(data === "paid"){
                                return `<span class="badge badge-success">Terverifikasi</span>`
                            }
                            else {
                                return `<span type="span" class="badge badge-warning">Menunggu Verifikasi</span>`
                            }
                        }
                    },
                    {   data: "payment_status",
                        render: function(data, type, full){
                            if(data === "paid"){
                                return `<div class="row justify-content-around">
                                            <div class="col align-item-center">
                                                <button type="button" class="btn btn-sm btn-danger" onclick="pesan(${full.ue_id})">Batal Verifikasi</button>
                                            </div>
                                            <div class="col align-item-center">
                                                <button type="button" class="btn btn-sm btn-danger" onclick="cancelEvent(${full.ue_id})">Cancel Event</button>
                                            </div>
                                        </div>`
                            }
                            else if (data === "not_paid"){
                                return `<div class="row justify-content-around">
                                            <div class="col align-item-center">
                                                <button type="button" class="btn btn-sm btn-primary disabled" data-toggle="modal" data-target="#staticBackdrop" disabled>Verifikasi</button>
                                            </div>
                                            <div class="col align-item-center">
                                                <button type="button" class="btn btn-sm btn-danger" onclick="cancelEvent(${full.ue_id})">Cancel Event</button>
                                            </div>
                                        </div>`
                            }
                            else{
                                return `<div class="row justify-content-around">
                                            <div class="col align-item-center">
                                                <button id="tes" data-id="${full.ue_id}" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-src="${full.bukti_bayar}" data-target="#staticBackdrop">Verifikasi</button>
                                            </div>
                                            <div class="col align-item-center">
                                                <button type="button" class="btn btn-sm btn-danger" onclick="cancelEvent(${full.ue_id})">Cancel Event</button>
                                            </div>
                                        </div>`
                            }
                        }
                    },
                ]
            });
             
            let xx = $("#tes").data("toggle");
        }
    })
    .fail(function(error) {
        console.log(error);
    });

    function cancelEvent(id) {
        $.ajax({
            method: 'DELETE',
            url: "/api/user-event/" +id
        }).done(function (response) {
            if(response.code == 200) {
                Swal.fire({
                    title: 'Success',
                    text: "Berhasil batalkan event!",
                    icon: 'success'
                    });
            }
            setTimeout(() => {
                location.reload();
            }, 1000);
        }).fail(function(error) {
            Swal.fire({
                    title: 'Error',
                    text: "Gagal batalkan event!",
                    icon: 'error'
                    });
            setTimeout(() => {
                location.reload();
            }, 1000);
        });
    }

    $('#staticBackdrop').on('show.bs.modal', function(e){
        let tombol = $(e.relatedTarget );
        let alamatGambar = tombol.data('src');
        let modal = $(this);
        let idVerif = tombol.data('id');
        modal.find('.modal-body img').attr('src', alamatGambar);
        modal.find('#tombolVerif').data('id', idVerif);
    });

    $('#tombolVerif').on('click', function(e){
        let id = $(this).data('id'); //ambil nilai dari attr data-id pada element dengan id="tombolVerif"
        
        $.ajax({
            method : "PuT",
            url: `/api/pembayaran/${id}/verified`,
        }).done(function(response){
            if(response.code == 200)  {
                Swal.fire({
                    title: 'Success',
                    text: "Berhasil verifikasi pembayaran!",
                    icon: 'success'
                    });
            }
            setTimeout(() => {
                location.reload();
            }, 1000);
            // MENGUPDATE DATA TABLE TANPA MERELOAD PAGES
            // window.reload()
            // error handling belum diatasi
            console.log(response);
        }).fail(function(error){
            console.log(error);
            Swal.fire({
                title: 'Gagal',
                text: "Gagal verifikasi pembayaran!",
                icon: 'error'
                });
            setTimeout(() => {
                location.reload();
            }, 1000);
        })
    })
  </script>
@endpush