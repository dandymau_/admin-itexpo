<!-- jQuery -->
<script src="{{asset('admin-lte')}}/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('admin-lte')}}/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- AdminLTE App -->
<script src="{{asset('admin-lte')}}/js/adminlte.js"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('admin-lte')}}/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('js/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('js')}}/main.js"></script>
@stack('js')
</body>
</html>