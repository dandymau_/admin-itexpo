<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//api route pembayaran
Route::get('/pembayaran', 'PembayaranController@index');
Route::put('/pembayaran/{id}/{status}', 'PembayaranController@update');

//api route untuk controller data peserta
Route::get('/peserta/{role}', 'PesertaController@index');
Route::get('/peserta/detail/{id}', 'PesertaController@show');
Route::get('/peserta/{id}/verifikasi', 'PesertaController@update');
Route::delete('/peserta/{id}', 'PesertaController@destroy');

//api route untuk mengambil peserta berdasarkan event
Route::get('/peserta/event/99', 'PesertaController@getUserByEvent');
Route::get('/peserta/event/{id}', 'PesertaController@getUserByEventId');

Route::delete('/submission/{id}', 'PesertaController@cancelSubmission');
Route::delete('/user-event/{id}', 'LombaController@cancelEvent');