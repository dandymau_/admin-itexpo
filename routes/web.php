<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', 'AuthController@loginView')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/daftar', function () {
    return view('register');
});

Route::post('/daftar', 'AuthController@register')->name('daftar');

Route::middleware('auth', 'verified')->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    
    Route::get('/peserta', function () {
        return view('peserta',[
            "title" => "peserta"
        ]);
    })->name('peserta');
    
    Route::get('/rekap-peserta', function () {
        return view('rekap-peserta', [
            "title" => "rekap-peserta"
        ]);
    });
    
    Route::get('/peserta/{id}/verifikasi', 'PesertaController@update');
    
    Route::get('/pembayaran', function () {
        return view('pembayaran', [
            "title" => "pembayaran"
        ]);
    });
    
    Route::get('/logout', 'AuthController@logoutFunc')->name('logout');
});


Route::get('/akun', function () {
    return Auth::user()->status_akun;
});