const notification = (type, text) => {
    swal({
        type,
        title: type === "success" ? "Berhasil!" : "Terjadi Kesalahan",
        text,
        // showConfrimButton: true,
        timer: 2000,
    });
};

// $("#form-login").submit(function (e) {
//     e.preventDefault();
//     $.ajax({
//         url: `http://localhost:8888/login`,
//         method: "POST",
//         data: $(this).serialize(),
//         success: function (response) {
//             console.log(response);
//             notification("success", response.message);
//             localStorage.setItem(KEY, response.data.token);
//             window.location = "/peserta";
//         },
//         error: function (err) {
//             console.log(err);
//             notification("error", err.responseJSON.message);
//         },
//     });
// });

$(document).ready(function () {
    var url = window.location;
    const allLinks = document.querySelectorAll(".nav-item a");
    const currentLink = [...allLinks].filter((e) => {
        return e.href == url;
    });
    if (currentLink[0].parentElement.classList.contains("level-2")) {
        currentLink[0].parentElement.parentElement.parentElement.classList.add(
            "menu-open"
        );
        currentLink[0].classList.add("active");
    } else {
        currentLink[0].classList.add("active");
    }
});
