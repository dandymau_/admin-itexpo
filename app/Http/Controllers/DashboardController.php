<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function index() {
        $data_siswa = DB::table('userx')
                    ->where('status_akun', 'pelajar')
                    ->where('is_delete', '0')
                    ->count();
        
        $data_mhs = DB::table('userx')
                    ->where('status_akun', 'mahasiswa')
                    ->where('is_delete', '0')
                    ->count();

        $data_umum = DB::table('userx')
                    ->where('status_akun', 'umum')
                    ->where('is_delete', '0')
                    ->count();

        $data_total_peserta = DB::table('userx')->where('status_akun', '!=', 'admin')->where('status_akun', '!=', 'super_admin')->where('is_delete', '0')->count();

        $data_not_paid = DB::table('userx')->join('userx_eventx', 'userx.id', '=', 'userx_eventx.userx_id')->select('userx.*', 'userx_eventx.payment_status')->where('userx_eventx.payment_status', 'not_paid')->where('userx_eventx.is_delete', '0')->count();

        $data_wait_verified = DB::table('userx')->join('userx_eventx', 'userx.id', '=', 'userx_eventx.userx_id')->select('userx.*', 'userx_eventx.payment_status')->where('userx_eventx.payment_status', 'wait_verified')->where('userx_eventx.is_delete', '0')->count();

        $data_paid = DB::table('userx')->join('userx_eventx', 'userx.id', '=', 'userx_eventx.userx_id')->select('userx.*', 'userx_eventx.payment_status')->where('userx_eventx.payment_status', 'paid')->where('userx_eventx.is_delete', '0')->count();

        $data_total_pembayaran = DB::table('userx')->join('userx_eventx', 'userx.id', '=', 'userx_eventx.userx_id')->select('userx.*', 'userx_eventx.payment_status')->where('userx.is_verified', '1')->where('payment_status', '!=', 'NULL')->where('userx_eventx.is_delete', '0')->count();

        $data_total_event = DB::table('userx_eventx')->where('is_delete', '0')->count();

        $data_lomba = DB::table('userx_eventx')->where('eventx_id', '!=', '6')->where('eventx_id', '!=', '7')->where('is_delete', '0')->count();

        $data_seminar = DB::table('userx_eventx')->where('eventx_id', '6')->where('is_delete', '0')->count();

        $data_workshop = DB::table('userx_eventx')->where('eventx_id', '7')->where('is_delete', '0')->count();

        return view('dashboard', [
            'title' => 'dashboard',
            'siswa' => $data_siswa,
            'mahasiswa' => $data_mhs,
            'umum' => $data_umum,
            'total_peserta' => $data_total_peserta,
            'not_paid' => $data_not_paid,
            'wait_verified' => $data_wait_verified,
            'paid' => $data_paid,
            'total_pembayaran' => $data_total_pembayaran,
            'lomba' => $data_lomba,
            'seminar' => $data_seminar,
            'workshop' => $data_workshop,
            'total_event' => $data_total_event
        ]);
    }
}
