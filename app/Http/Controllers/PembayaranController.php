<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PembayaranController extends Controller
{
    public function index() {
        $data = DB::table('userx_eventx')
                ->join('eventx', 'userx_eventx.eventx_id', '=', 'eventx.id')
                ->join('userx','userx_eventx.userx_id', '=', 'userx.id')
                ->select('userx_eventx.id as ue_id', 'eventx.id as e_id', 'userx.nama as nama_lengkap', 'eventx.name as nama_event', 'userx.instansi', 'userx.kontak', 'userx_eventx.bukti_bayar', 'userx_eventx.payment_status')
                ->where('userx_eventx.is_delete', '0')
                ->get();

        
        if(!$data->isEmpty()) {
            return response()->json([
                'code' => 200,
                'message' => 'data ditemukan',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'code' => 400,
                'message' => 'data tidak ditemukan',
                'data' => array()
            ]);
        }
    }

    public function update($id, $status) {
        $data = DB::table('userx_eventx')->select('userx_id', 'eventx_id')
                    ->where('is_delete', '0')
                    ->where('id', $id)->first();

        switch ($status) {
            case 'cancel':
                DB::beginTransaction();
                
                $result = DB::table('userx_eventx')
                ->where('id', $id)
                ->update([
                    'payment_status' => 'not_paid'
                ]);

                if($data->eventx_id == 4) {
                    $result2 = DB::table('userx_eventx')
                    ->where('eventx_id', 8)->where('userx_id', $data->userx_id)
                    ->where('is_delete', '0')
                    ->update([
                        'payment_status' => 'not_paid'
                    ]);
                }

                DB::commit();
                $msg = "Berhasil hapus status";
                break;
            
            case 'verified': 
                DB::beginTransaction();

                $result = DB::table('userx_eventx')
                ->where('id', $id)
                ->update([
                    'payment_status' => 'paid'
                ]);

                if($data->eventx_id == 4) {
                    $result2 = DB::table('userx_eventx')
                    ->where('eventx_id', 8)->where('userx_id', $data->userx_id)
                    ->where('is_delete', '0')
                    ->update([
                        'payment_status' => 'paid'
                    ]);
                }

                DB::commit();
                $msg = "Berhasil verifikasi pembayaran";
                break;
        }

        if($result ==  1) {
            return response()->json([
                'code' => 200,
                'message' => $msg
            ]);
        } else {
            return response()->json([
                'code' => 400,
                'message' => 'Gagal update',
                'data' => [$result, $data->eventx_id]
            ]);
        }
    }
}
