<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class AuthController extends Controller
{
    public function login(Request $request) {
        $email = $request->input('email');
        $password = $request->input('password');

        if(Auth::attempt(['email' => $email, 'password' => $password, 'status_akun' => ['admin', 'super_admin']])) {
            return redirect()->intended('dashboard');
        } else {
            return back()->with('failed', 'Email/Password salah!');
        }
        
    }

    public function loginView() {
        if(Auth::user()) {
            return redirect()->route('dashboard');
        } else {
            return view('login', [
                "title" => 'login'
            ]);
        }
    }

    public function register(Request $request) {
        $input = DB::table('userx')->insert([
            'email' => $request->input('email'),
            'nama' => $request->input('nama'),
            'password' => Hash::make($request->input('password')),
            'tanggal_lahir' => date("Y-m-d"),
            'status_akun' => 'admin',
            'instansi' => 'UNJ',
            'kontak' => '082123456789',
            'is_verified' => '0',
            'file_ktm' => '',
            'foto' => '',
            'token' => '',
            'token_verifikasi' => '',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        if ($input == '1') {
            return redirect()->route('login');
        } else {
            return back()->with('failed', 'Gagal registrasi');
        }
    }

    public function logoutFunc() {
        Auth::logout();

        if(Auth::user()) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login');
        }
    }
}
