<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class PesertaController extends Controller
{
    public function index($role) {

        if ($role == 'super_admin') {
            $data = DB::table('userx')->where('is_delete', 0)->latest()->get();
        } else {
            $data = DB::table('userx')->where('is_delete', 0)->where('status_akun', '!=', 'super_admin')->where('status_akun', '!=', 'admin')->get();
        }

        if(!$data->isEmpty()) {
            return response()->json([
                'code' => 200,
                'data' => $data
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'data' => array()
            ]);
        }
    }

    public function update(Request $request, $id) {
        $peserta = DB::table('userx')->where('id', $id)->first();
        $verified_at = date("Y-m-d h:i:s");
        if($peserta->is_verified == '0') {
            try {
                DB::beginTransaction();
        
                DB::table('userx')->where('id', $id)->update([
                    'is_verified' => 1,
                    'token_verifikasi' => "",
                    'verified_at' => $verified_at
                ]);
        
                DB::commit();
                return back()->with('success','Sukses verifikasi peserta');
            } catch(Exception $e) {
                DB::rollback();
                return back()->with('failed','Gagal verifikasi peserta');
            }
        } else {
            return back()->with('success', 'Peserta terverifikasi');
        }
    }

    public function show($id) {
        $data = DB::table('userx_eventx')
                ->join('userx', 'userx_eventx.userx_id', '=', 'userx.id')
                ->join('eventx', 'userx_eventx.eventx_id', '=', 'eventx.id')
                ->select('userx.id', 'userx.nama', 'userx.tanggal_lahir', 'userx.status_akun', 'userx.instansi', 'userx.kontak', 'userx.email','eventx.name as nama_event')->get();
        
        return response()->json([
            'data' => $data
        ]);
    }

    public function getUserByEventId($id) {
        $data = DB::table('userx_eventx')
                ->join('eventx', 'eventx.id', '=', 'userx_eventx.eventx_id')
                ->join('userx', 'userx.id', '=', 'userx_eventx.userx_id')
                ->leftJoin('submitx', 'submitx.userx_eventx_id', '=', 'userx_eventx.id')
                ->select('userx_eventx.id', 'userx.nama', 'userx.status_akun', 'userx.instansi', 'userx.kontak', 'userx_eventx.submission', 'submitx.file', 'submitx.link', 'submitx.created_at', 'userx_eventx.eventx_id', 'eventx.name as nama_event')
                ->where('userx_eventx.eventx_id', $id)
                ->where('userx_eventx.is_delete', '0')
                ->get();

        if(!$data->isEmpty()) {
            return response()->json([
                'code' => 200,
                'message' => 'data ditemukan',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'code' => 404,
                'message' => 'data tidak ditemukan',
                'data' => $data
            ]);
        }
    }

    public function getUserByEvent() {
        $data = DB::table('userx_eventx')
                ->join('eventx', 'eventx.id', '=', 'userx_eventx.eventx_id')
                ->join('userx', 'userx.id', '=', 'userx_eventx.userx_id')
                ->leftJoin('submitx', 'submitx.userx_eventx_id', '=', 'userx_eventx.id')
                ->select('userx_eventx.id', 'userx.nama', 'userx.status_akun', 'userx.instansi', 'userx.kontak', 'userx_eventx.submission', 'submitx.file', 'submitx.link', 'submitx.created_at', 'userx_eventx.eventx_id', 'eventx.name as nama_event')
                ->where('userx_eventx.is_delete', '0')
                ->get();

        if(!$data->isEmpty()) {
            return response()->json([
                'code' => 200,
                'message' => 'data peserta lomba ditemukan',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'code' => 404,
                'message' => 'tidak ada peserta lomba',
                'data' => array()
            ]);
        }
    }

    public function cancelSubmission($id) {
        DB::beginTransaction();

        $update = DB::table('userx_eventx')->where('id', $id)
                ->update([
                    'submission' => 0
                ]);

        $hapus = DB::table('submitx')->where('userx_eventx_id', $id)
                ->delete();

        if($update == 1 && $hapus == 1) {
            DB::commit();
            return response()->json([
                'code' => 200,
                'message' => 'Sukses batalkan submisi'
            ]);
        } else {
            DB::rollback();
            return response()->json([
                'code' => 400,
                'message' => 'Gagal batalkan submisi'
            ]);
        }
    }

    public function destroy(Request $request ,$id) {
        $role = $request->getContent();
        // return response()->json([
        //     'code' => 200,
        //     'data' => $role
        // ]);
        if($role == 'super_admin') {
            DB::beginTransaction();
    
            $user = DB::table('userx')->where('id', $id)->where('is_delete', 0)->get();
    
            if($user->isNotEmpty()) {
                $destroy = DB::table('userx')->where('id', $id)->update([
                    'is_delete' => 1,
                ]);
    
                DB::commit();
    
                return response()->json([
                    'code' => 200,
                    'message' => 'Berhasil hapus akun'
                ]);
            } else {
                DB::rollback();
    
                return response()->json([
                    'code' => 400,
                    'message' => 'Gagal hapus akun'
                ]);
            }
        } else {
            return response()->json([
                'code' => 400,
                'message' => 'Anda bukan super admin'
            ]);
        }
    }
}
