<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LombaController extends Controller
{
    public function index() {
        // $data = DB::table('submitx')->
    }

    public function show($id) {

    }

    public function cancelEvent($id) {
        try {
            //code...
            DB::beginTransaction();
            $data = DB::table("userx_eventx")->where('id', $id)->update([
                'is_delete' => "1"
            ]);
            if($data) {
                DB::commit();
                return response()->json([
                    'code' => 200,
                    'message' => 'Berhasil batalkan event'
                ]);
            } else {
                DB::rollback();
                return response()->json([
                    'code' => 400,
                    'message' => 'Gagal batalkan event'
                ]);
            }
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            return response()->json([
                'code' => 400,
                'message' => 'Gagal batalkan event'
            ]);
        }
        

    }
}
