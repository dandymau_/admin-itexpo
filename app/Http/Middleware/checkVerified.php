<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class checkVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->is_verified == 0) {
            Auth::logout();
            return redirect()->route('login')->with('failed', 'Akun belum terverifikasi');
        }

        return $next($request);
    }
}
